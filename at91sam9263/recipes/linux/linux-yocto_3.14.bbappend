KBRANCH_at91sam9263ek ?= "standard/arm-versatile-926ejs"
SRCREV_machine_at91sam9263ek = "1a9f9edade8d8c0120d2f47dfd15edd41c7a3439"
COMPATIBLE_MACHINE_at91sam9263ek = "at91sam9263ek"

FILESEXTRAPATHS_prepend := "${THISDIR}/linux-yocto:"

SRC_URI_append_at91sam9263ek := " \ 
								file://defconfig"

