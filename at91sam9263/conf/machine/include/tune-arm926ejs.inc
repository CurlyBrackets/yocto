FEED_ARCH = "armv5"
PACKAGE_EXTRA_ARCHS += "armv4 armv4t armv5"
# For gcc 3.x you need:
#TARGET_CC_ARCH = "-march=armv5 -mtune=arm926ejs"
# For gcc 4.x you need:
TARGET_CC_ARCH = "-march=armv5 -mtune=arm926ej-s"
BASE_PACKAGE_ARCH = "armv5"
